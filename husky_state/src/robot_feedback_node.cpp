/*
 *
 *  Created on: Sep. 19, 2021
 *      Author: hsiuchin
 */

#include <ros/ros.h>
#include <gazebo_msgs/GetModelState.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>

int main(int argc, char** argv) {

	ros::init(argc, argv, "robot_state_node");
  	ros::NodeHandle node_handle_("~");


/*
	if (ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug)) {
		ros::console::notifyLoggerLevelsChanged();
	}
*/
	int publish_rate = 1000 ;
	ros::Rate rate(publish_rate) ;

	ROS_DEBUG_STREAM("Starting to robot_state_node");


	// read robot state from gazebo
	gazebo_msgs::GetModelState 	get_model_state_srv ;
	get_model_state_srv.request.model_name 				= "husky" ;
	get_model_state_srv.request.relative_entity_name 	= "world" ;
	ros::ServiceClient model_state_client = node_handle_.serviceClient<gazebo_msgs::GetModelState>("/gazebo/get_model_state") ;

	geometry_msgs::Pose pose_ ;
	geometry_msgs::Twist twist_ ;

	std::string pose_topic_name_ = "/husky_velocity_controller/feedback/pose" ;
	std::string twist_topic_name_ = "/husky_velocity_controller/feedback/twist" ;
	ros::Publisher pose_publisher_ = node_handle_.advertise<geometry_msgs::Pose> (pose_topic_name_, 1) ;
	ros::Publisher twist_publisher_ = node_handle_.advertise<geometry_msgs::Twist>(twist_topic_name_, 1) ;

	while ( ros::ok() ) {

		// read the current pose
		if ( model_state_client.call(get_model_state_srv) ) {

			pose_  = get_model_state_srv.response.pose ;
			twist_ = get_model_state_srv.response.twist ;
			ROS_DEBUG_STREAM("[robot_state_node] " << pose_);
			ROS_DEBUG_STREAM("[robot_state_node] " << twist_);
			pose_publisher_.publish(pose_) ;
			twist_publisher_.publish(twist_) ;
		}
		else {
			ROS_DEBUG_STREAM("[robot_state_node] waiting for gazebo");
		}

		ros::spinOnce();
		rate.sleep();
	}

	return 0 ;
}


